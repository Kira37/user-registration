package steps

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *RegistrationSteps) RegisteringWithValidUsernameEmailAndPassword(username, email, password string) error {
	s.testUsername = username
	req, _ := http.NewRequest(http.MethodPost, "/register", strings.NewReader(fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, username, email, password)))
	req.Header.Set("Content-Type", "application/json")

	s.router.ServeHTTP(s.recorder, req)

	return nil
}

func (s *RegistrationSteps) SubmitTheRegistrationForm() error {
	return nil
}

func (s *RegistrationSteps) RegisterTheUserSuccessfully() error {
	want := "user registered successfully"
	got := s.recorder.Body.String()
	pass := strings.Contains(got, want)

	if !pass {
		return fmt.Errorf("want: %s, got: %s", want, got)
	}

	if s.recorder.Code != http.StatusOK {
		return fmt.Errorf("expected status 200 but got %d", s.recorder.Code)
	}

	return nil
}

func InitializeValidScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &RegistrationSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^I am registering with valid username "([^"]*)", email "([^"]*)", and password "([^"]*)"$`, steps.RegisteringWithValidUsernameEmailAndPassword)
	ctx.Step(`^I submit the registration form$`, steps.SubmitTheRegistrationForm)
	ctx.Step(`^the system should register the user successfully$`, steps.RegisterTheUserSuccessfully)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupRegisterTestUser()
	})
}
