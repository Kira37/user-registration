Feature: Invalid Email Format Handling

  Scenario: Register with an invalid email format
    Given I am registering with username "invaliduser", invalid email "invalidemail", and password "ValidPass1!"
    When I submit the registration form with invalid email
    Then the system should return an error message indicating that the email format is invalid
