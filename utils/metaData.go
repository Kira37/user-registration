package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-management/models"
)

func MetaDataHandler(c *gin.Context, data interface{}, status string, message string) {
	resp := models.Response{
		MetaData: map[string]interface{}{
			"status":  status,
			"message": message,
		},
		Data: data,
	}

	c.JSON(http.StatusOK, resp)
}
