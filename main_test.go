package main

import (
	"os"
	"testing"

	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
	"gitlab.com/Kira37/user-registration/steps"
)

func TestMain(m *testing.M) {
	opts := godog.Options{
		Format: "pretty",
		Output: colors.Colored(os.Stdout),
	}

	suite := godog.TestSuite{
		Name: "user-registration",
		ScenarioInitializer: func(ctx *godog.ScenarioContext) {
			steps.InitializeValidScenario(ctx)
			steps.InitializeDuplicateNameScenario(ctx)
			steps.InitializeShortUsernameScenario(ctx)
			steps.InitializeWeakPasswordScenario(ctx)
			steps.InitializeInvalidEmailScenario(ctx)

			steps.InitializeValidLoginScenario(ctx)
			steps.InitializeInvalidUsernameLoginScenario(ctx)
			steps.InitializeInvalidPasswordLoginScenario(ctx)

		},
		Options: &opts,
	}

	status := suite.Run()

	os.Exit(status)
}
