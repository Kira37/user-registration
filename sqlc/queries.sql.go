// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.27.0
// source: queries.sql

package sqlc

import (
	"context"
	"time"
)

const createUser = `-- name: CreateUser :exec
INSERT INTO users (username, email, password) VALUES ($1, $2, $3)
`

type CreateUserParams struct {
	Username string
	Email    string
	Password string
}

func (q *Queries) CreateUser(ctx context.Context, arg CreateUserParams) error {
	_, err := q.db.ExecContext(ctx, createUser, arg.Username, arg.Email, arg.Password)
	return err
}

const deleteRefreshToken = `-- name: DeleteRefreshToken :exec
DELETE FROM refresh_tokens WHERE token = $1
`

func (q *Queries) DeleteRefreshToken(ctx context.Context, token string) error {
	_, err := q.db.ExecContext(ctx, deleteRefreshToken, token)
	return err
}

const deleteUserByUsername = `-- name: DeleteUserByUsername :exec
DELETE FROM users WHERE username = $1
`

func (q *Queries) DeleteUserByUsername(ctx context.Context, username string) error {
	_, err := q.db.ExecContext(ctx, deleteUserByUsername, username)
	return err
}

const findRefreshToken = `-- name: FindRefreshToken :one
SELECT token, username, expires_at FROM refresh_tokens WHERE token = $1
`

func (q *Queries) FindRefreshToken(ctx context.Context, token string) (RefreshToken, error) {
	row := q.db.QueryRowContext(ctx, findRefreshToken, token)
	var i RefreshToken
	err := row.Scan(&i.Token, &i.Username, &i.ExpiresAt)
	return i, err
}

const findUserByUsername = `-- name: FindUserByUsername :one
SELECT username, password, email FROM users WHERE username = $1
`

type FindUserByUsernameRow struct {
	Username string
	Password string
	Email    string
}

func (q *Queries) FindUserByUsername(ctx context.Context, username string) (FindUserByUsernameRow, error) {
	row := q.db.QueryRowContext(ctx, findUserByUsername, username)
	var i FindUserByUsernameRow
	err := row.Scan(&i.Username, &i.Password, &i.Email)
	return i, err
}

const isEmailRegistered = `-- name: IsEmailRegistered :one
SELECT COUNT(*) FROM users WHERE email = $1
`

func (q *Queries) IsEmailRegistered(ctx context.Context, email string) (int64, error) {
	row := q.db.QueryRowContext(ctx, isEmailRegistered, email)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const isUsernameTaken = `-- name: IsUsernameTaken :one
SELECT COUNT(*) FROM users WHERE username = $1
`

func (q *Queries) IsUsernameTaken(ctx context.Context, username string) (int64, error) {
	row := q.db.QueryRowContext(ctx, isUsernameTaken, username)
	var count int64
	err := row.Scan(&count)
	return count, err
}

const storeRefreshToken = `-- name: StoreRefreshToken :exec
INSERT INTO refresh_tokens (token, username, expires_at) VALUES ($1, $2, $3)
`

type StoreRefreshTokenParams struct {
	Token     string
	Username  string
	ExpiresAt time.Time
}

func (q *Queries) StoreRefreshToken(ctx context.Context, arg StoreRefreshTokenParams) error {
	_, err := q.db.ExecContext(ctx, storeRefreshToken, arg.Token, arg.Username, arg.ExpiresAt)
	return err
}
