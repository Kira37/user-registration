package service

import (
	"net/http"

	"github.com/joomcode/errorx"
)

var errorNamespace = errorx.NewNamespace("service_errors")

var (
	ConflictError = errorNamespace.NewType("conflict_error")
)

var ErrorStatusMap = map[*errorx.Type]int{
	ConflictError: http.StatusConflict,
}
