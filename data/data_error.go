package data

import (
	"net/http"

	"github.com/joomcode/errorx"
)

var errorNamespace = errorx.NewNamespace("data_errors")

var (
	DatabaseError     = errorNamespace.NewType("database_error")
	NotFoundError     = errorNamespace.NewType("not_found_error")
	UnauthorizedError = errorNamespace.NewType("unauthorized_error")
)

var ErrorStatusMap = map[*errorx.Type]int{
	DatabaseError:     http.StatusInternalServerError,
	NotFoundError:     http.StatusNotFound,
	UnauthorizedError: http.StatusUnauthorized,
}
