package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/utils"
)

func getErrorType(err error) *errorx.Type {
	if e, ok := err.(*errorx.Error); ok {
		return e.Type()
	}
	return nil
}

func ErrorHandling() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			err := c.Errors.Last().Err

			var statusCode int
			var errorMsg string

			errType := getErrorType(err)
			if errType != nil {
				if status, ok := service.ErrorStatusMap[errType]; ok {
					statusCode = status
				} else if status, ok := handler.ErrorStatusMap[errType]; ok {
					statusCode = status
				} else if status, ok := data.ErrorStatusMap[errType]; ok {
					statusCode = status
				} else if status, ok := utils.ErrorStatusMap[errType]; ok {
					statusCode = status
				} else {
					statusCode = http.StatusInternalServerError
				}
				errorMsg = err.Error()
			} else {
				statusCode = http.StatusInternalServerError
				errorMsg = "internal server error"
			}

			c.JSON(statusCode, gin.H{
				"error": errorMsg,
			})
		}
	}
}
