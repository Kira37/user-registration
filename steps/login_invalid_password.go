package steps

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *LoginSteps) AttemptingToLogInWithAnInvalidPassword() error {
	err := s.aRegisteredUserWithValidCredentials()
	if err != nil {
		return err
	}

	payload := fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, s.testUsername, s.testEmail, "K@ir57ubel")

	req, err := http.NewRequest(http.MethodPost, "/login", strings.NewReader(payload))
	if err != nil {
		return fmt.Errorf("error creating login request: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	s.recorder = httptest.NewRecorder()
	s.router.ServeHTTP(s.recorder, req)

	return nil
}

func (s *LoginSteps) LogInWithInvalidPassword() error {
	return nil
}

func (s *LoginSteps) ErrorMessageIndicatingThatThePasswordIsIncorrect() error {
	want := "password mismatch"
	got := s.recorder.Body.String()
	pass := strings.Contains(got, want)

	if !pass {
		return fmt.Errorf("want: %s, got: %s", want, got)
	}

	if s.recorder.Code != http.StatusUnauthorized {
		return fmt.Errorf("expected status code %d, got %d", http.StatusUnauthorized, s.recorder.Code)
	}

	var response map[string]interface{}
	err := json.Unmarshal(s.recorder.Body.Bytes(), &response)
	if err != nil {
		return fmt.Errorf("error parsing response body: %v", err)
	}

	return nil
}

func InitializeInvalidPasswordLoginScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &LoginSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^I am attempting to log in with an invalid password$`, steps.AttemptingToLogInWithAnInvalidPassword)
	ctx.Step(`^I log in with my username and password$`, steps.LogInWithInvalidPassword)
	ctx.Step(`^the system should return an error message indicating that the password is incorrect$`, steps.ErrorMessageIndicatingThatThePasswordIsIncorrect)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupLoginTestUser()
	})
}
