package steps

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *RegistrationSteps) UserWithTheUsernameIsAlreadyRegistered(username string) error {

	req, _ := http.NewRequest(http.MethodPost, "/register", strings.NewReader(fmt.Sprintf(`{"username":"%s","email":"%s@example.com","password":"ValidPass1!"}`, username, username)))
	req.Header.Set("Content-Type", "application/json")

	s.router.ServeHTTP(s.recorder, req)

	return nil
}

func AttemptToRegisterWithTheSameUsername() error {
	return nil
}

func (s *RegistrationSteps) ErrorMessageIndicatingThatTheUsernameAlreadyExists() error {
	want := "username already exists"
	got := s.recorder.Body.String()
	pass := strings.Contains(got, want)

	if !pass {
		return fmt.Errorf("want: %s, got: %s", want, got)
	}

	if s.recorder.Code != http.StatusConflict {
		return fmt.Errorf("expected 409 but got %v", s.recorder.Code)

	}
	return nil
}

func InitializeDuplicateNameScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &RegistrationSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^a user with the username "([^"]*)" is already registered$`, steps.UserWithTheUsernameIsAlreadyRegistered)
	ctx.Step(`^I attempt to register with the same username$`, AttemptToRegisterWithTheSameUsername)
	ctx.Step(`^the system should return an error message indicating that the username already exists$`, steps.ErrorMessageIndicatingThatTheUsernameAlreadyExists)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupRegisterTestUser()
	})
}
