Feature: Login with Valid Credentials

  Scenario: User logs in with valid credentials
    Given I am a registered user with valid credentials
    When I log in with my username and password
    Then the system should generate a JWT token for authentication and issue a refresh token
