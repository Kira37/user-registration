package steps

import (
	"context"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/middleware"
	"go.uber.org/zap"
)

type RegistrationSteps struct {
	userHandler  *handler.UserHandler
	userRepo     *data.UserRepository
	recorder     *httptest.ResponseRecorder
	router       *gin.Engine
	testUsername string
}

func (s *RegistrationSteps) Setup() {
	s.router = gin.New()
	s.router.Use(middleware.ErrorHandling())
	s.router.POST("/register", s.userHandler.Register)
	s.recorder = httptest.NewRecorder()
}

func (s *RegistrationSteps) cleanupRegisterTestUser() {
	if s.testUsername != "" {
		err := s.userRepo.DeleteUserByUsername(context.Background(), s.testUsername)
		if err != nil {
			s.userHandler.Logger.Error("failed to clean up user after test", zap.String("username", s.testUsername), zap.Error(err))
		}
	}
}
