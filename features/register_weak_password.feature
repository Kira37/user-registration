Feature: Weak Password Handling

  Scenario: Register with a weak password
    Given I am registering with username "weakpassuser", email "weakpassuser@example.com", and weak password "123"
    When I submit the registration form with weak password
    Then the system should return an error message indicating that the password is not strong enough
