package steps

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *LoginSteps) aRegisteredUserWithValidCredentials() error {
	username := "testingName"
	email := "testing@gmail.com"
	password := "Testing@11"

	payload := fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, username, email, password)

	s.testUsername = username
	s.testEmail = email
	s.testPassword = password

	req, err := http.NewRequest(http.MethodPost, "/register", strings.NewReader(payload))
	if err != nil {
		return fmt.Errorf("error creating request: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	s.router.ServeHTTP(s.recorder, req)
	return nil
}

func (s *LoginSteps) LogInWithValidUsernameAndPassword() error {
	payload := fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, s.testUsername, s.testEmail, s.testPassword)
	req, err := http.NewRequest(http.MethodPost, "/login", strings.NewReader(payload))
	if err != nil {
		return fmt.Errorf("error creating login request: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	s.recorder = httptest.NewRecorder()
	s.router.ServeHTTP(s.recorder, req)
	return nil
}

func (s *LoginSteps) GenerateAJWTTokenForAuthenticationAndIssueARefreshToken() error {
	want := "logged in successfully"
	got := s.recorder.Body.String()
	pass := strings.Contains(got, want)

	if !pass {
		return fmt.Errorf("want: %s, got: %s", want, got)
	}

	if s.recorder.Code != http.StatusOK {
		return fmt.Errorf("expected status code %d, got %d", http.StatusOK, s.recorder.Code)
	}

	var response map[string]interface{}
	err := json.Unmarshal(s.recorder.Body.Bytes(), &response)
	if err != nil {
		return fmt.Errorf("error parsing response body: %v", err)
	}

	data, ok := response["data"].(map[string]interface{})
	if !ok {
		return fmt.Errorf("data field not found in response")
	}

	if _, ok := data["access_token"]; !ok {
		return fmt.Errorf("access token not found in response")
	}

	if _, ok := data["refresh_token"]; !ok {
		return fmt.Errorf("refresh token not found in response")
	}

	return nil
}

func InitializeValidLoginScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &LoginSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^I am a registered user with valid credentials$`, steps.aRegisteredUserWithValidCredentials)
	ctx.Step(`^I log in with my username and password$`, steps.LogInWithValidUsernameAndPassword)
	ctx.Step(`^the system should generate a JWT token for authentication and issue a refresh token$`, steps.GenerateAJWTTokenForAuthenticationAndIssueARefreshToken)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupLoginTestUser()
	})
}
