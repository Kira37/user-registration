package handler

import (
	"net/http"

	"github.com/joomcode/errorx"
)

var errorNamespace = errorx.NewNamespace("handler_errors")

var (
	BadRequestError = errorNamespace.NewType("bad_request")
	ValidationError = errorNamespace.NewType("validation_error")
)

var ErrorStatusMap = map[*errorx.Type]int{
	BadRequestError: http.StatusBadRequest,
	ValidationError: http.StatusBadRequest,
}
