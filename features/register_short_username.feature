Feature: Username Length Requirement

  Scenario: Register with a short username
    Given I am registering with short username "usr", email "user@example.com", and password "ValidPass1!"
    When I submit the registration form with short username
    Then the system should return an error message indicating that the username must be at least 5 characters long
