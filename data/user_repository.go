package data

import (
	"context"
	"database/sql"
	"time"

	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-registration/models"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

type UserRepositoryInterface interface {
	IsUsernameTaken(ctx context.Context, username string) (bool, *errorx.Error)
	IsEmailRegistered(email string, ctx context.Context) (bool, *errorx.Error)
	CreateUser(user *models.User, ctx context.Context) *errorx.Error
	FindUserByUsername(ctx context.Context, username string) (*models.User, *errorx.Error)
	StoreRefreshToken(rft *models.RefreshToken, ctx context.Context) *errorx.Error
	FindRefreshToken(tokenString string, ctx context.Context) (string, *errorx.Error)
}

type UserRepository struct {
	dbQueries *sqlc.Queries
	logger    *zap.Logger
}

func NewUserRepository(dbQueries *sqlc.Queries, logger *zap.Logger) *UserRepository {
	return &UserRepository{
		dbQueries: dbQueries,
		logger:    logger,
	}
}

func (r *UserRepository) FindUserByUsername(ctx context.Context, username string) (*models.User, *errorx.Error) {
	user, err := r.dbQueries.FindUserByUsername(ctx, username)
	if err != nil {
		if err == sql.ErrNoRows {
			r.logger.Warn("invalid username", zap.String("username", username))
			return nil, NotFoundError.Wrap(err, "invalid username")
		}
		r.logger.Error("error querying user data", zap.Error(err))
		return nil, DatabaseError.Wrap(err, "error querying user data")
	}
	return &models.User{Username: user.Username, Password: user.Password, Email: user.Email}, nil
}

func (r *UserRepository) IsUsernameTaken(ctx context.Context, username string) (bool, *errorx.Error) {
	count, err := r.dbQueries.IsUsernameTaken(ctx, username)
	if err != nil {
		r.logger.Error("database query error while checking username", zap.String("username", username), zap.Error(err))
		return false, DatabaseError.Wrap(err, "database query error while checking username: "+username)
	}
	return count > 0, nil
}

func (r *UserRepository) IsEmailRegistered(email string, ctx context.Context) (bool, *errorx.Error) {
	count, err := r.dbQueries.IsEmailRegistered(ctx, email)
	if err != nil {
		r.logger.Error("database query error while checking email", zap.String("email", email), zap.Error(err))
		return false, DatabaseError.Wrap(err, "database query error while checking email: "+email)
	}
	return count > 0, nil
}

func (r *UserRepository) CreateUser(user *models.User, ctx context.Context) *errorx.Error {
	err := r.dbQueries.CreateUser(ctx, sqlc.CreateUserParams{
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	})
	if err != nil {
		r.logger.Error("failed to insert user into database", zap.Error(err))
		return DatabaseError.Wrap(err, "failed to insert user into database")
	}
	return nil
}

func (r *UserRepository) StoreRefreshToken(rft *models.RefreshToken, ctx context.Context) *errorx.Error {
	err := r.dbQueries.StoreRefreshToken(ctx, sqlc.StoreRefreshTokenParams{
		Token:     rft.Token,
		Username:  rft.Username,
		ExpiresAt: rft.ExpiresAt,
	})
	if err != nil {
		r.logger.Error("failed to insert refresh token into database", zap.Error(err))
		return DatabaseError.Wrap(err, "failed to insert refresh token into database")
	}
	return nil
}

func (r *UserRepository) FindRefreshToken(tokenString string, ctx context.Context) (string, *errorx.Error) {
	rft, err := r.dbQueries.FindRefreshToken(ctx, tokenString)
	if err != nil {
		if err == sql.ErrNoRows {
			r.logger.Warn("invalid token", zap.String("token", tokenString))
			return "", NotFoundError.Wrap(err, "invalid token")
		}
		r.logger.Error("error querying token data", zap.Error(err))
		return "", DatabaseError.Wrap(err, "error querying token data")
	}

	if rft.ExpiresAt.Before(time.Now()) {
		r.logger.Warn("refresh token expired", zap.String("token", tokenString))
		return "", UnauthorizedError.Wrap(err, "refresh token expired")
	}

	err = r.dbQueries.DeleteRefreshToken(ctx, tokenString)
	if err != nil {
		r.logger.Error("failed to delete refresh token", zap.Error(err))
		return "", DatabaseError.Wrap(err, "failed to delete refresh token")
	}

	return rft.Username, nil
}

func (r *UserRepository) DeleteUserByUsername(ctx context.Context, username string) *errorx.Error {
	err := r.dbQueries.DeleteUserByUsername(ctx, username)
	if err != nil {
		r.logger.Error("failed to delete user from database", zap.String("username", username), zap.Error(err))
		return DatabaseError.Wrap(err, "failed to delete user from database")
	}
	return nil
}
