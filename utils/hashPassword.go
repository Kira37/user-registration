package utils

import (
	"github.com/joomcode/errorx"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string, logger *zap.Logger) (string, *errorx.Error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		logger.Error("error hashing password", zap.Error(err))
		return "", errorx.InternalError.Wrap(err, "failed to hash password")
	}

	return string(hashedPassword), nil
}
