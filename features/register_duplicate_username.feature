Feature: Duplicate Username Handling

  Scenario: Register with a duplicate username
    Given a user with the username "testuser" is already registered
    When I attempt to register with the same username
    Then the system should return an error message indicating that the username already exists
