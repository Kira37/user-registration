Feature: Login with Invalid Username

  Scenario: Attempt to log in with an invalid username
    Given I am attempting to log in with an invalid username
    When I submit the login form
    Then the system should return an error message indicating that the username is not registered
