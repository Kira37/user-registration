package utils

import (
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-registration/models"
	"go.uber.org/zap"
)

func GenerateAccessToken(username string, expirationTime time.Duration, logger *zap.Logger) (string, *errorx.Error) {
	expiration := time.Now().Add(expirationTime)
	claims := &models.Claims{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiration.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(os.Getenv("secret_key")))
	if err != nil {
		logger.Error("error generating access token", zap.String("username", username), zap.Error(err))
		return "", AccessTokenError.Wrap(err, "failed to generate access token")
	}

	return tokenString, nil
}

func GenerateRefreshToken(expirationTime time.Duration, logger *zap.Logger) (string, *errorx.Error) {
	expiration := time.Now().Add(expirationTime)

	claims := &jwt.StandardClaims{
		ExpiresAt: expiration.Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte(os.Getenv("refresh_secret_key")))
	if err != nil {
		logger.Error("error generating refresh token", zap.Error(err))
		return "", AccessTokenError.Wrap(err, "failed to generate refresh token")
	}

	return tokenString, nil
}
