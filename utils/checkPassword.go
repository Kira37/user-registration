package utils

import (
	"errors"
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-registration/models"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

func Validate(u models.User) error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required, validation.Length(5, 0), validation.Match(regexp.MustCompile("^[a-zA-Z0-9]+$"))),
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Password, validation.Required, validation.Length(8, 0), validation.By(ValidatePassword)),
	)
}

func ValidatePassword(value interface{}) error {
	str, ok := value.(string)
	if !ok {
		return errors.New("invalid type, expected string")
	}

	var (
		upperRegex   = regexp.MustCompile(`[A-Z]`)
		lowerRegex   = regexp.MustCompile(`[a-z]`)
		numberRegex  = regexp.MustCompile(`\d`)
		specialRegex = regexp.MustCompile(`[!@#~$%^&*(),.?":{}|<>]`)
	)

	if !upperRegex.MatchString(str) {
		return errors.New("password must contain at least one uppercase letter")
	}
	if !lowerRegex.MatchString(str) {
		return errors.New("password must contain at least one lowercase letter")
	}
	if !numberRegex.MatchString(str) {
		return errors.New("password must contain at least one digit")
	}
	if !specialRegex.MatchString(str) {
		return errors.New("password must contain at least one special character")
	}

	return nil
}

func ComparePasswords(hashedPassword []byte, plainPassword []byte, logger *zap.Logger) *errorx.Error {
	err := bcrypt.CompareHashAndPassword(hashedPassword, plainPassword)
	if err != nil {
		logger.Error("password mismatch", zap.Error(err))
		return UnauthorizedError.Wrap(err, "password mismatch")
	}
	return nil
}
