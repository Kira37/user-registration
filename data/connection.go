package data

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

var DB *sql.DB

func ConnectDB() {
	var err error
	DB, err = sql.Open("postgres", os.Getenv("DB_URL"))
	if err != nil {
		log.Fatalf("Failed to open the database: %v", err)
	}

	err = DB.Ping()
	if err != nil {
		log.Fatalf("Failed to connect to the database: %v", err)
	}

	fmt.Println("Successfully connected to database")
}

func CreateTables() {
	createUsersTableQuery := `
    CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        username VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        created_at TIMESTAMPTZ DEFAULT NOW()
    );
	`

	createRefreshTokensTableQuery := `
    CREATE TABLE IF NOT EXISTS refresh_tokens (
        token VARCHAR(255) PRIMARY KEY,
        username VARCHAR(255) NOT NULL,
        expires_at TIMESTAMPTZ NOT NULL,
        FOREIGN KEY (username) REFERENCES users (username) ON DELETE CASCADE
    );
	`

	_, err := DB.Exec(createUsersTableQuery)
	if err != nil {
		log.Fatalf("Error creating users table: %v", err)
	}

	_, err = DB.Exec(createRefreshTokensTableQuery)
	if err != nil {
		log.Fatalf("Error creating refresh_tokens table: %v", err)
	}

	fmt.Println("Users and Refresh Tokens tables created or already exist")
}
