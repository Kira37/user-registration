Feature: Register with Valid Credentials

  Scenario: Register with valid credentials
    Given I am registering with valid username "validuser", email "validuser@example.com", and password "ValidPass1!"
    When I submit the registration form
    Then the system should register the user successfully
