package steps

import (
	"context"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/middleware"
	"go.uber.org/zap"
)

type LoginSteps struct {
	router       *gin.Engine
	recorder     *httptest.ResponseRecorder
	userHandler  *handler.UserHandler
	userRepo     *data.UserRepository
	testUsername string
	testEmail    string
	testPassword string
}

func (s *LoginSteps) Setup() {
	s.router = gin.New()
	s.router.Use(middleware.ErrorHandling())
	s.router.POST("/register", s.userHandler.Register)
	s.router.POST("/login", s.userHandler.Login)
	s.recorder = httptest.NewRecorder()
}

func (s *LoginSteps) cleanupLoginTestUser() {
	if s.testUsername != "" {
		err := s.userRepo.DeleteUserByUsername(context.Background(), s.testUsername)
		if err != nil {
			s.userHandler.Logger.Error("failed to clean up user after test", zap.String("username", s.testUsername), zap.Error(err))
		}
	}
}
