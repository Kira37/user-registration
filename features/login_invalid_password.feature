Feature: Login with Invalid Password

  Scenario: Attempt to log in with an invalid password
    Given I am attempting to log in with an invalid password
    When I submit the login form
    Then the system should return an error message indicating that the password is incorrect
