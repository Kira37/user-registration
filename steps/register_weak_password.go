package steps

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *RegistrationSteps) RegisteringWithUsernameEmailAndWeakPassword(username, email, password string) error {
	s.testUsername = username
	req, _ := http.NewRequest(http.MethodPost, "/register", strings.NewReader(fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, username, email, password)))
	req.Header.Set("Content-Type", "application/json")

	s.router.ServeHTTP(s.recorder, req)

	return nil
}

func (s *RegistrationSteps) SubmitTheRegistrationFormWithWeakPassword() error {
	return nil
}

func (s *RegistrationSteps) ErrorMessageIndicatingThatThePasswordIsNotStrongEnough() error {
	passwordValidationErrors := []string{
		"password: the length must be no less than 8",
		"password must contain at least one uppercase letter",
		"password must contain at least one lowercase letter",
		"password must contain at least one digit",
		"password must contain at least one special character",
	}

	got := s.recorder.Body.String()
	var foundError string

	for _, errMsg := range passwordValidationErrors {
		if strings.Contains(got, errMsg) {
			foundError = errMsg
			break
		}
	}

	if foundError == "" {
		return fmt.Errorf("expected one of the password validation errors but got: %s", got)
	}

	if s.recorder.Code != http.StatusBadRequest {
		return fmt.Errorf("expected status 400 but got %d", s.recorder.Code)
	}
	return nil
}

func InitializeWeakPasswordScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &RegistrationSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^I am registering with username "([^"]*)", email "([^"]*)", and weak password "([^"]*)"$`, steps.RegisteringWithUsernameEmailAndWeakPassword)
	ctx.Step(`^I submit the registration form with weak password$`, steps.SubmitTheRegistrationFormWithWeakPassword)
	ctx.Step(`^the system should return an error message indicating that the password is not strong enough$`, steps.ErrorMessageIndicatingThatThePasswordIsNotStrongEnough)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupRegisterTestUser()
	})
}
