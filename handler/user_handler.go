package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-registration/models"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/utils"
	"go.uber.org/zap"
)

type UserHandler struct {
	UserService service.UserServiceInterface
	Logger      *zap.Logger
}

func NewUserHandler(userService service.UserServiceInterface, logger *zap.Logger) *UserHandler {
	return &UserHandler{UserService: userService, Logger: logger}
}

func (h *UserHandler) Register(c *gin.Context) {
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		h.Logger.Error("invalid request body", zap.Error(err))
		c.Error(BadRequestError.Wrap(err, "invalid request body"))
		return
	}

	if err := utils.Validate(user); err != nil {
		h.Logger.Error("validation failed", zap.Error(err))
		c.Error(ValidationError.Wrap(err, "validation failed"))
		return
	}

	registeredUser, err := h.UserService.RegisterUser(user.Username, user.Email, user.Password, c.Request.Context())
	if err != nil {
		c.Error(err)
		return
	}

	h.Logger.Info("user registered successfully")
	utils.MetaDataHandler(c, registeredUser, "success", "user registered successfully")

}

func (h *UserHandler) Login(c *gin.Context) {
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		h.Logger.Error("invalid request body", zap.Error(err))
		c.Error(BadRequestError.Wrap(err, "invalid request body"))
		return
	}

	authenticatedUser, err := h.UserService.AuthenticateUser(user.Username, user.Password, c.Request.Context())
	if err != nil {
		c.Error(err)
		return
	}

	tokens, err := h.UserService.GenerateTokens(user.Username, c.Request.Context())
	if err != nil {
		c.Error(err)
		return
	}

	h.Logger.Info("user logged in successfully")
	utils.MetaDataHandler(c, tokens, "success", "welcome "+authenticatedUser.Username+", logged in successfully")

}

func (h *UserHandler) RefreshToken(c *gin.Context) {
	var refreshToken models.RefreshToken

	if err := c.ShouldBindJSON(&refreshToken); err != nil {
		h.Logger.Error("invalid request body", zap.Error(err))
		c.Error(BadRequestError.Wrap(err, "invalid request body"))
		return
	}

	username, err := h.UserService.ValidateAndInvalidateRefreshToken(refreshToken.Token, c.Request.Context())
	if err != nil {
		c.Error(err)
		return
	}

	tokens, err := h.UserService.GenerateTokens(username, c.Request.Context())
	if err != nil {
		c.Error(err)
		return
	}

	h.Logger.Info("token refreshed successfully")
	utils.MetaDataHandler(c, tokens, "success", "token refreshed successfully")

}
