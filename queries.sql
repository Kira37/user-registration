
-- name: FindUserByUsername :one
SELECT username, password, email FROM users WHERE username = $1;

-- name: IsUsernameTaken :one
SELECT COUNT(*) FROM users WHERE username = $1;

-- name: IsEmailRegistered :one
SELECT COUNT(*) FROM users WHERE email = $1;

-- name: CreateUser :exec
INSERT INTO users (username, email, password) VALUES ($1, $2, $3);

-- name: StoreRefreshToken :exec
INSERT INTO refresh_tokens (token, username, expires_at) VALUES ($1, $2, $3);

-- name: FindRefreshToken :one
SELECT token, username, expires_at FROM refresh_tokens WHERE token = $1;

-- name: DeleteRefreshToken :exec
DELETE FROM refresh_tokens WHERE token = $1;

-- name: DeleteUserByUsername :exec
DELETE FROM users WHERE username = $1;
