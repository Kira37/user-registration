package main

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/Kira37/user-registration/config"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/middleware"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func init() {
	config.LoadEnv()
	data.ConnectDB()
	data.CreateTables()
}

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("failed to initialize logger: %v", err)
	}
	defer logger.Sync()

	dbQueries := sqlc.New(data.DB)
	userRepository := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepository, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	router := gin.Default()

	router.Use(middleware.TimeoutMiddleware(15*time.Second, logger))
	router.Use(middleware.ErrorHandling())

	router.POST("/register", userHandler.Register)
	router.POST("/login", userHandler.Login)
	router.POST("/refresh", userHandler.RefreshToken)

	if err := router.Run(":8080"); err != nil {
		logger.Fatal("failed to start server", zap.Error(err))
	}
}
