package service

import (
	"context"
	"time"

	"github.com/joomcode/errorx"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/models"
	"gitlab.com/Kira37/user-registration/utils"
	"go.uber.org/zap"
)

type UserServiceInterface interface {
	RegisterUser(username, email, password string, ctx context.Context) (*models.User, *errorx.Error)
	AuthenticateUser(username, password string, ctx context.Context) (*models.User, *errorx.Error)
	GenerateTokens(username string, ctx context.Context) (map[string]string, *errorx.Error)
	ValidateAndInvalidateRefreshToken(tokenString string, ctx context.Context) (string, *errorx.Error)
}

type UserService struct {
	userRepository data.UserRepositoryInterface
	logger         *zap.Logger
}

func NewUserService(userRepository data.UserRepositoryInterface, logger *zap.Logger) *UserService {
	return &UserService{userRepository: userRepository, logger: logger}
}

func (s *UserService) RegisterUser(username, email, password string, ctx context.Context) (*models.User, *errorx.Error) {
	isUsernameTaken, err := s.userRepository.IsUsernameTaken(ctx, username)
	if err != nil {
		return nil, err
	}

	if isUsernameTaken {
		s.logger.Warn("username already exists", zap.String("username", username))
		return nil, ConflictError.New("username already exists")
	}

	isEmailRegistered, err := s.userRepository.IsEmailRegistered(email, ctx)
	if err != nil {
		return nil, err
	}

	if isEmailRegistered {
		s.logger.Warn("email already registered", zap.String("email", email))
		return nil, ConflictError.New("email already registered")
	}

	hashedPassword, err := utils.HashPassword(password, s.logger)
	if err != nil {
		return nil, err
	}

	user := &models.User{
		Username: username,
		Email:    email,
		Password: hashedPassword,
	}

	if err := s.userRepository.CreateUser(user, ctx); err != nil {
		return nil, err
	}

	return user, nil
}

func (s *UserService) AuthenticateUser(username, password string, ctx context.Context) (*models.User, *errorx.Error) {
	user, err := s.userRepository.FindUserByUsername(ctx, username)
	if err != nil {
		return nil, err
	}

	err = utils.ComparePasswords([]byte(user.Password), []byte(password), s.logger)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *UserService) GenerateTokens(username string, ctx context.Context) (map[string]string, *errorx.Error) {
	accessToken, err := utils.GenerateAccessToken(username, 15*time.Minute, s.logger)
	if err != nil {
		return nil, err
	}

	refreshToken, err := utils.GenerateRefreshToken(30*24*time.Hour, s.logger)
	if err != nil {
		return nil, err
	}

	refreshTokenRecord := models.RefreshToken{
		Token:     refreshToken,
		Username:  username,
		ExpiresAt: time.Now().Add(30 * 24 * time.Hour),
	}

	if err := s.userRepository.StoreRefreshToken(&refreshTokenRecord, ctx); err != nil {
		return nil, err
	}

	return map[string]string{
		"access_token":  accessToken,
		"refresh_token": refreshToken,
	}, nil
}

func (s *UserService) ValidateAndInvalidateRefreshToken(tokenString string, ctx context.Context) (string, *errorx.Error) {
	return s.userRepository.FindRefreshToken(tokenString, ctx)
}
