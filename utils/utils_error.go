package utils

import (
	"net/http"

	"github.com/joomcode/errorx"
)

var errorNamespace = errorx.NewNamespace("util_errors")

var (
	UnauthorizedError = errorNamespace.NewType("unauthorized_error")
	AccessTokenError  = errorNamespace.NewType("unauthorized_error")
)

var ErrorStatusMap = map[*errorx.Type]int{
	AccessTokenError:  http.StatusInternalServerError,
	UnauthorizedError: http.StatusUnauthorized,
}
