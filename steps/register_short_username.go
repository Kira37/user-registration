package steps

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/cucumber/godog"
	"gitlab.com/Kira37/user-registration/data"
	"gitlab.com/Kira37/user-registration/handler"
	"gitlab.com/Kira37/user-registration/service"
	"gitlab.com/Kira37/user-registration/sqlc"
	"go.uber.org/zap"
)

func (s *RegistrationSteps) RegisteringWithShortUsernameEmailAndPassword(username, email, password string) error {
	s.testUsername = username
	req, _ := http.NewRequest(http.MethodPost, "/register", strings.NewReader(fmt.Sprintf(`{"username":"%s","email":"%s","password":"%s"}`, username, email, password)))
	req.Header.Set("Content-Type", "application/json")

	s.router.ServeHTTP(s.recorder, req)

	return nil
}

func (s *RegistrationSteps) SubmitTheRegistrationFormWithShortUsername() error {
	return nil
}

func (s *RegistrationSteps) ErrorMessageIndicatingThatTheUsernameMustBeAtLeast5CharactersLong() error {
	want := "username: the length must be no less than 5"
	got := s.recorder.Body.String()
	pass := strings.Contains(got, want)

	if !pass {
		return fmt.Errorf("want: %s, got: %s", want, got)
	}

	if s.recorder.Code != http.StatusBadRequest {
		return fmt.Errorf("expected status 400 but got %d", s.recorder.Code)
	}
	return nil
}

func InitializeShortUsernameScenario(ctx *godog.ScenarioContext) {
	logger, _ := zap.NewDevelopment()
	dbQueries := sqlc.New(data.DB)
	userRepo := data.NewUserRepository(dbQueries, logger)
	userService := service.NewUserService(userRepo, logger)
	userHandler := handler.NewUserHandler(userService, logger)

	steps := &RegistrationSteps{
		userHandler: userHandler,
		userRepo:    userRepo,
	}

	steps.Setup()

	ctx.Step(`^I am registering with short username "([^"]*)", email "([^"]*)", and password "([^"]*)"$`, steps.RegisteringWithShortUsernameEmailAndPassword)
	ctx.Step(`^I submit the registration form with short username$`, steps.SubmitTheRegistrationFormWithShortUsername)
	ctx.Step(`^the system should return an error message indicating that the username must be at least 5 characters long$`, steps.ErrorMessageIndicatingThatTheUsernameMustBeAtLeast5CharactersLong)

	ctx.AfterScenario(func(*godog.Scenario, error) {
		steps.cleanupRegisterTestUser()
	})
}
